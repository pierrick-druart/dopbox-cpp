GREEN		=	\033[0;32m
ORANGE		=	\033[0;35m
BLUE		=	\033[0;34m
CYAN		=	\033[0;36m
NCOLOR		=	\033[0m

SCRIPTCONFIG	=	Config.sh

all	:	config

config	:	
		./$(SCRIPTCONFIG)
		@mv -f $(SCRIPTCONFIG) ./Extras
		@echo "$(CYAN)Move\t\t$(NCOLOR)[$(SCRIPTCONFIG)] to ./Extras"
		@echo "$(ORANGE)Done$(NCOLOR)\t\t[init Database]"
		@echo "$(GREEN)Type make in the terminal to finish the install :$(NCOLOR)"
		@echo "$(GREEN)- 'make' for compile all the project$(NCOLOR)"
		@echo "$(GREEN)- 'make mycloud' for compile Mycloud$(NCOLOR)"
		@echo "$(GREEN)- 'make lib' for compile lib_database$(NCOLOR)"
		@mv -f ./Extras/Makefile .
#!/bin/bash
# Copyright Pierrick DRUART
#script init all database and user for MyCloud project

echo "/* MyCloud Database Configurator */"

echo "Enter your root mysql login :"
read rootLogin

echo "Enter your root mysql password :"
read rootPassword

echo "Enter your database name :"
read database

echo "Enter your user login :"
read login

echo "Enter your user password :"
read password

rm -rf .MyCloud/MyCloud.config

echo "[MyCloud Root Config]
rootLogin= $rootLogin
rootPassword= $rootPassword
database= $database
[MyCloud Root Config]" > .MyCloud/MyCloud.config

mysql -u $rootLogin -p$rootPassword << EOF
GRANT ALL PRIVILEGES ON *.* TO '$login'@'localhost' IDENTIFIED BY '$password' WITH GRANT OPTION;
CREATE DATABASE IF NOT EXISTS $database;
USE $database;
CREATE TABLE IF NOT EXISTS user ( id MEDIUMINT AUTO_INCREMENT, login VARCHAR(80) NOT NULL, password VARCHAR(80), PRIMARY KEY (id));  
INSERT IGNORE INTO user VALUES(NULL, '$login','$password');
SELECT * FROM user;
QUIT
EOF


#ifndef SQLITE_HH__
#define SQLITE_HH__

#include <string>
#include <sqlite3.h>

class SQLite 
{
private:
	sqlite3	*db;

	/* Constructor */
public:
	SQLite();
	SQLite(const std::string & database);
	virtual ~SQLite();

/* Public Method */
public:
	const std::string getUser(const std::string & login, const std::string & password);
};

#endif /* SQLITE_HH__ */

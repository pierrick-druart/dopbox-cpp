#ifndef DATABASETYPE_HH__
#define DATABASETYPE_HH__

#include <string>

template<typename _DataBaseType>
class DataBaseType
{
private:
    _DataBaseType   *_db;

    /* Constructor */
public:
    DataBaseType();
    DataBaseType(const std::string & database);
    DataBaseType(const std::string & user, const std::string & password, const std::string & database);
    virtual ~DataBaseType();

    /* Public Method */
public:
    const std::string getUser(const std::string & login, const std::string & password);
};

#endif /* __DATABASETYPE_HH__ */

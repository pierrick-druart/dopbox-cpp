#include "Info.hpp"

/* CONSTRUCTOR & DESTRUCTOR */

Info::Info() {}

Info::Info(std::string nom, std::string prenom)
{
  this->nom = nom;
  this->prenom = prenom;
}

Info::~Info() {}

void Info::setNom(std::string nom)
{
  this->nom = nom;
}

void Info::setPrenom(std::string prenom)
{
  this->prenom = prenom;
}

std::string Info::getNom()
{
  return this->nom;
}

std::string Info::getPrenom()
{
  return this->prenom;
}

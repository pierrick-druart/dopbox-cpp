/*
 * main.cpp
 *
 *  Created on: 28 oct. 2014
 *      Author: nicolas
 */

#include <string.h>
#include <iostream>

#include <boost/lexical_cast.hpp>

#include "model/info.hpp"
#include "service/server.hpp"
#include "service/client.hpp"
#include <vector>
#include <fstream>


namespace mycloud {

int run_server()	{
	try	{
		unsigned short port = boost::lexical_cast<unsigned short>(5554);
		boost::asio::io_service io_service;
		service::server server(io_service, port);
		io_service.run();
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
	}
	return 0;
}

int run_client(const std::vector<mycloud::model::info> & infos)	{
	try	{
		boost::asio::io_service io_service;
		model::info i;
		service::client client(infos, io_service, "127.0.0.1", "5554");
		io_service.run();
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
	}
	return 0;
}
}

int main(int argc, char ** argv)	{
	if (strcmp(argv[1], "client")==0)	{
		std::cout << "client" << std::endl;

		std::vector<mycloud::model::info> infos;

		mycloud::model::info iFirst;
		iFirst.setNom(argv[2]);
		iFirst.setPrenom(argv[3]);

		mycloud::model::info iSecond;
		iSecond.setNom(argv[4]);
		iSecond.setPrenom(argv[5]);

		infos.push_back(iFirst);
		infos.push_back(iSecond);

		mycloud::run_client(infos);
	} else {
		std::cout << "server" << std::endl;
		mycloud::run_server();
	}
}

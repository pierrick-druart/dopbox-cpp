# MYCLOUD README #

This README would normally document whatever steps are necessary to get your application up and running.

# What is this repository for? #

* This project aims to reproduce the Dropbox service in cpp language
* V1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

# First of all #

## This project is not entirely finished. He can still receive improvements and modifications, including: ##
* Optimization of thread
* Optimization of XML serialization
* Optimization of client off
* And other

## For the rest type 'help' and guided you through the different options. ##

# How to work MyCloud ? #

* First launch

When you first launch the application, you must type in the terminal first 'make'. This one will take care of setting up your database mysql for MyCloud app:
- Create your user for the application MyCloud
- Create the necessary database has MyCloud
- Upgrade of user rights
- Import the installation Makefile
- Verification of already existent database to avoid duplication

Configuration :

![Capture d’écran 2014-12-26 à 15.04.38.png](https://bitbucket.org/repo/7XXqRk/images/3493569163-Capture%20d%E2%80%99%C3%A9cran%202014-12-26%20%C3%A0%2015.04.38.png)

* Installation

After configuring the mysql database complete, you can launch for a second time the command 'make'.
The Makefile will take care this time to compile the database Librairy MyCloud and finally compile the project. At this complation will organize the distribution of different files (.so, .hpp) in the different projects files.

All compilation:

![Capture d’écran 2014-12-26 à 15.10.22.png](https://bitbucket.org/repo/7XXqRk/images/1472678397-Capture%20d%E2%80%99%C3%A9cran%202014-12-26%20%C3%A0%2015.10.22.png)

Librairies compilation:

![Capture d’écran 2014-12-26 à 15.07.55.png](https://bitbucket.org/repo/7XXqRk/images/2367955657-Capture%20d%E2%80%99%C3%A9cran%202014-12-26%20%C3%A0%2015.07.55.png)

Project compilation:

![Capture d’écran 2014-12-26 à 15.08.58.png](https://bitbucket.org/repo/7XXqRk/images/166289928-Capture%20d%E2%80%99%C3%A9cran%202014-12-26%20%C3%A0%2015.08.58.png)

# Different MyCloud Usage Scenario #

## Scenario 1 ##
### Client and Server Startup : ###
* Service Server, run './MyCloud Server' in your terminal. Once done press 'start' to start the MyCloud server.
* Client Service, run './MyCloud Client' in your terminal. Staying by typing 'login' then your login and password. Once done press 'start' to start the MyCloud client.
* The server must be running to perform operations from the client.

### Recovery of repos cloud server : ###
* In the 'start' of the customer, it will automatically do a query of nearby server to retrieve the repos.
* You can also enter at any time 'synchronize' in the client to update the repos.

### - Comparison of repos: ###
* When the client recovers a referentiel it will automatically compared it has its own repos, and place the result in the client log.

### - Viewing the log of the comparison: ###
* You can type 'log' in the client at any time to see the last lines recorded log. The log in this .MyCloud / MyCloud.client

## Scenario 2 ##
* Add files directly within the specified Client directory has this effect.
* Notify will detect the event. The events supported for this version are CREATE, DELETE, MODIFY.
* The client will send the file directly to the server as a file compressed .gz.
* The server will store the file in the expected Server directory.
* You can see by typing 'log' in the client or server, receiving or sending the file.

## Scenario 3 ##
* the update of the client from the server referential works has little meadows.

## Scenario 4 ##
* Start and stop server : works.
* Start and Stop client : works has little meadows.

# How do I get set up? #

* Configuration

Launch Config.sh to set up the MyCloud Database.

* Dependencies

Sqlite3 :

```
#!shell

sudo apt-get install sqlite3
```

MySQL:

```
#!shell

sudo apt-get install mysql-client mysql-common mysql-server libmysqlcppconn-dev
```

Boost :

```
#!shell

sudo apt-get install libboost-all-dev
```

LibXml :

```
#!shell

apt-get install libxml2-dev libxslt1-dev python-dev
```

All its dependence is normally already integrated in the Makefile MyCloud and can run the compilation even if they are missing.

* Database configuration

Config.sh :

```
#!sh

#!/bin/bash                                                                                                                                                                                                        
# Copyright Pierrick DRUART                                                                                                                                                                                        
#script init all database and user for MyCloud project                                                                                                                                                             

echo "/* MyCloud Database Configurator */"

echo "Enter your root mysql login :"
read rootLogin

echo "Enter your root mysql password :"
read rootPassword

echo "Enter your user login :"
read login

echo "Enter your user password :"
read password

mysql -u $rootLogin -p$rootPassword << EOF                                                                                                                                                                         
GRANT ALL PRIVILEGES ON *.* TO '$login'@'localhost' IDENTIFIED BY '$password' WITH GRANT OPTION;                                                                                                                   
CREATE DATABASE IF NOT EXISTS mycloud;                                                                                                                                                                             
USE mycloud;                                                                                                                                                                                                       
CREATE TABLE IF NOT EXISTS user ( id MEDIUMINT AUTO_INCREMENT, login VARCHAR(80) NOT NULL, password VARCHAR(80), PRIMARY KEY (id));                                                                                
INSERT IGNORE INTO user VALUES(NULL, '$login','$password');                                                                                                                                                        
SELECT * FROM user;                                                                                                                                                                                                
QUIT                                                                                                                                                                                                               
EOF
```

when running this script you have to go to him the various information for the database configuration is optimal:
- MySQL root login
- MySQL root password
- Login want in MyCloud
- Password want in MyCloud

### Makefile ###

* Makefile Configuration :


```
#!Makefile

GREEN           =       \033[0;32m
ORANGE          =       \033[0;35m
BLUE            =       \033[0;34m
CYAN            =       \033[0;36m
NCOLOR          =       \033[0m

SCRIPTCONFIG    =       Config.sh

all     :       config

config  :
                ./$(SCRIPTCONFIG)
                @mv -f $(SCRIPTCONFIG) ./Extras
                @echo "$(CYAN)Move\t\t$(NCOLOR)[$(SCRIPTCONFIG)] to ./Extras"
                @echo "$(ORANGE)Done$(NCOLOR)\t\t[init Database]"
                @echo "$(GREEN)Type make in the terminal to finish the install :$(NCOLOR)"
                @echo "$(GREEN)- 'make' for compile all the project$(NCOLOR)"
                @echo "$(GREEN)- 'make mycloud' for compile Mycloud$(NCOLOR)"
                @echo "$(GREEN)- 'make lib' for compile lib_database$(NCOLOR)"
                @mv -f ./Extras/Makefile .
```
This Makefile intended to start the database configuration script and import the installation makefile once the configuration is complete

* Makefile Installation : 

```
#!Makefile

GREEN		=	\033[0;32m
ORANGE		=	\033[0;35m
BLUE		=	\033[0;34m
CYAN		=	\033[0;36m
NCOLOR		=	\033[0m

NAME		=	MyCloud

LIBNAME		=	lib_database.so

CC		=	g++ -std=c++11

SRC		=	./Src/main.cpp					\
			./Src/MyCloud.cpp				\
			./Src/Service/Notify/Notify.cpp			\
			./Src/Service/Server/CommunicationServer.cpp	\
			./Src/Service/Client/CommunicationClient.cpp	\
			./Src/ViewController/Server/Server.cpp		\
			./Src/ViewController/Client/Client.cpp		\
			./Src/Manager/Archive/ArchiveManager.cpp	\
			./Src/Manager/Xml/XmlManager.cpp		\
			./Src/Manager/Stream/StreamManager.cpp		\
			./Src/Manager/Log/LogManager.cpp		\
			./Src/Model/Info/Info.cpp			\

SRC_LIB 	=	./Src/SrcLib/MySQL.cpp				\
			./Src/SrcLib/SQLite.cpp				\

OBJ		=	$(SRC:.cpp=.o)

CFLAGS		=	#-Wall -Wextra -W #DEBUG

FLAGBOOST	=	-lboost_serialization -lboost_system -lpthread

FLAGXML		=	-I./Includes/libxml2/

FLAGSQLITE	=	-I./Includes/sqlite3/

FLAGMYSQL	=	-I./Includes/mysql/

FLAGDBTYPE	=	-I./Includes/database/

LIBSQLITE	=	-L. ./Lib/lib_sqlite/*.so

LIBMYSQL	=	-L. ./Lib/lib_mysql/*.so

LIBXML		=	-L. ./Lib/lib_xml/libxml2.so

LIBBOOST	=	-L. ./Lib/lib_boost/*.so

LIBDBTYPE	=	-L. ./Lib/lib_database/*.so

all	:	$(LIBNAME)	\
		$(NAME)		\


$(NAME)	:	$(OBJ)
		@$(CC) $(LIBXML) $(LIBBOOST) $(LIBDBTYPE) -o $(NAME) $(OBJ) $(FLAGBOOST) $(FLAGXML) $(FLAGDBTYPE)
		@echo "$(BLUE)Linked$(NCOLOR)\t\t[$(NAME)]"
		@echo "$(GREEN)Type './MyCloud Client' in terminal for launch client$(NCOLOR)"
		@echo "$(GREEN)Type './MyCloud Server' in terminal for launch server$(NCOLOR)"
		@echo "$(ORANGE)Done$(NCOLOR)\t\t[cleaning *.o]"

$(LIBNAME):	$(OBJ_LIB)
		@$(CC) -o $(LIBNAME) $(SRC_LIB) -fPIC -shared $(LIBSQLITE) $(LIBMYSQL) $(FLAGSQLITE) $(FLAGMYSQL)
		@echo "$(BLUE)Linked$(NCOLOR)\t\t[$(LIBNAME)]"
		@mv -f $(LIBNAME) ./Lib/lib_database/
		@echo "$(CYAN)Copy\t\t$(NCOLOR)[lib_database/*.hpp] to Includes/database."
		@cp -f ./Src/SrcLib/*.hpp ./Includes/database/
		@echo "$(CYAN)Move\t\t$(NCOLOR)[$(LIBNAME)] to lib/lib_database."
		@rm -f ./Src/SrcLib/*.o
		@echo "$(ORANGE)Done$(NCOLOR)\t\t[cleaning *.o]"

%.o	:	%.cpp
		@$(CC) $(CFLAGS) $(FLAGBOOST) $(FLAGXML) $(FLAGDBTYPE) -c $< -o $@
		@echo "$(GREEN)Compilated$(NCOLOR)\t[$<]"

clean	:
		@rm -f *#
		@rm -f *~
		@rm -f ./Src/*#
		@rm -f ./Src/*~
		@rm -f ./Src/SrcLib/*~
		@rm -f ./src/SrcLib/*#
		@rm -f ./Src/Service/Notify/*~
		@rm -f ./Src/Service/Notify/*#
		@rm -f ./Src/Model/Info/*~
		@rm -f ./Src/Model/Info/*#
		@rm -f ./Src/InfoFile/*~
		@rm -f ./Src/InfoFile/*#
		@rm -f ./Src/Serializer/*~
		@rm -f ./Src/Serializer/*#
		@rm -f ./Src/Service/Server/*~
		@rm -f ./Src/Service/Server/*#
		@rm -f ./Src/Service/Client/*~
		@rm -f ./Src/Service/Client/*#
		@rm -f ./Src/ViewController/Server/*~
		@rm -f ./Src/ViewController/Server/*#
		@rm -f ./Src/ViewController/Client/*~
		@rm -f ./Src/ViewController/Client/*#
		@rm -f ./Src/Manager/Archive/*~
		@rm -f ./Src/Manager/Archive/*#
		@rm -f ./Src/Manager/Log/*~
		@rm -f ./Src/Manager/Log/*#
		@rm -f ./Src/Manager/Xml/*~
		@rm -f ./Src/Manager/Xml/*#
		@rm -f ./Src/Manager/Stream/*~
		@rm -f ./Src/Manager/Stream/*#
		@rm -f ./Src/Utils/*~
		@rm -f ./Src/Utils/*#
		@rm -f *.o
		@rm -f ./Src/*.o
		@rm -f ./Src/Service/Notify/*.o
		@rm -f ./Src/Model/Info/*.o
		@rm -f ./Src/Model/InfoFile/*.o
		@rm -f ./Src/Serializer/*.o
		@rm -f ./Src/Service/Server/*.o
		@rm -f ./Src/Service/Client/*.o
		@rm -f ./Src/Service/Notify/*.o
		@rm -f ./Src/ViewController/Server/*.o
		@rm -f ./Src/ViewController/Client/*.o
		@rm -f ./Src/Manager/Archive/*.o
		@rm -f ./Src/Manager/Xml/*.o
		@rm -f ./Src/Manager/Stream/*.o
		@rm -f ./Src/Manager/Log/*.o
		@echo "$(ORANGE)Done$(NCOLOR)\t\t[clean]"

fclean	:	clean
		@rm -f $(NAME)
		@echo "$(ORANGE)Done$(NCOLOR)\t\t[fclean]"

lib	:	$(LIBNAME)

mycloud	:	$(NAME)

re	:	fclean all
```
This installation Makefile will compile the sources of MyCloud project with different libraries and flag needed a proper compilation of the project.
 In particular can be found at the project root sets the libraries needed to run a local compilation.
In addition, it will load the cleaning and distributing the folder various required files (such as lib_database.so and includes).


# Intelligent processing #

### You can see in the application that all treatments are with pointers to member function ###
* this technique is deployed in the majority of the data processing

```
#!c++

void Client::initEventParser()
{
  this->__func["login"]      	= &Client::eventLoginClient;
  this->__func["disconnect"]   	= &Client::eventDisconnectClient;
  this->__func["start"]   	= &Client::eventStartNotifyClient;
  this->__func["stop"]   	= &Client::eventStopNotifyClient;
  this->__func["status"]   	= &Client::eventStatusClient;
  this->__func["createuser"]	= &Client::eventCreateUserClient;
  this->__func["log"]		= &Client::eventLogClient;
  this->__func["help"]		= &Client::eventHelpClient;
  this->__func["exit"]		= &Client::eventExitClient;
}

 std::map<std::string, Function>::iterator	__it;

  for (__it = this->__func.begin(); __it != this->__func.end(); ++__it)
    if ((*__it).first == __cmd)
	(this->*__func[__cmd])();

private:
typedef void (Client:: *Function)();
  std::map<std::string, Function>	       	__func;

  void				eventHelpClient();
  void				eventCreateUserClient();
  void				eventLoginClient();
  void				eventDisconnectClient();
  void				eventStartNotifyClient();
  void				eventStopNotifyClient();
  void				eventStatusClient();
  void				eventLogClient();
  void				eventExitClient();

```

# Threading #

## Client ##
The client will thread the Notify class avoided blocking the main loop. Then when an action is detected by Notify, this one will come create as many threads as there are notifications

## Server ##
The server will thread the CommunicationServer class avoided blocking the main loop. This thread will be able to receive packets while continuing the manipulated

# Who do I talk to? #

* Repo of Pierrick DRUART
* Project of Ryzoom School
* For Nicolas Kaszuba


# Thanks to  Nicolas Kaszuba and Happy New Year #
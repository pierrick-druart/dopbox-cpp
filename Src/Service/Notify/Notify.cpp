#include "Notify.hpp"

Notify::Notify(int refreshTime,
	       const std::string &id,
	       LogManager *logManager, 
	       StreamManager *streamManager)
{
  this->__refreshTime = refreshTime;
  this->__id = id;
  this->__logManager = logManager;
  this->__streamManager = streamManager;
  this->__xmlManager = new XmlManager();
  this->__archiveManager = new ArchiveManager();
  this->__home = this->__logManager->getHome() + PATH_CLIENT;


  boost::thread notify(&Notify::startNotify, this);
  this->__threadNotify = &notify;
}

Notify::~Notify() 
{
  std::cout << "deleting notifier" << std::endl;
  /*inotify_rm_watch(this->__fd, this->__wd);
  close(this->__fd);*/
}

void Notify::setRefreshTime(int refreshTime)
{
  this->__refreshTime = refreshTime;
}

void Notify::setListenning(bool listenning)
{
  this->__listenning = listenning;
}
 
const int Notify::getRefreshTime() const
{
  return (this->__refreshTime);
}

void Notify::init()
{
  this->__fd   	= inotify_init();
  this->__wd   	= inotify_add_watch(this->__fd, 
				    this->__home.c_str(), 
				    IN_MOVED_TO | 
				    IN_MOVED_FROM | 
				    IN_CREATE |
				    IN_DELETE |
				    IN_MODIFY |
				    IN_OPEN |
				    IN_ACCESS);
}


void Notify::initEventParser()
{
  //this->__funcEvent[IN_MOVED_TO]	= &Notify::eventMoveToFile;
  //this->__funcEvent[IN_MOVED_FROM]	= &Notify::eventMoveFromFile;
  this->__funcEvent[IN_CREATE]		= &Notify::eventCreateFile;
  this->__funcEvent[IN_DELETE]		= &Notify::eventDeleteFile;
  //this->__funcEvent[IN_MODIFY]		= &Notify::eventModifyFile;
  //this->__funcEvent[IN_OPEN]		= &Notify::eventOpenFile;
  //this->__funcEvent[IN_ACCESS]		= &Notify::eventAccesFile;
}

void Notify::startNotify()
{
  this->init();
  this->initEventParser();
  this->__communicationClient = new CommunicationClient(this->__logManager, 
							this->__streamManager, 
							this->__xmlManager);
  this->__listenning = true;

  this->getReferentielPacket(REFERENTIEL);

  while(this->__listenning)
    this->notifyEventListener();

  inotify_rm_watch(this->__fd, this->__wd);
  close(this->__fd);
  delete this;
}

void Notify::stopNotify()
{
  this->__communicationClient->stopClientCommunication();

}

void Notify::notifyEventListener()
{
  struct inotify_event	*__event;
  char			*__p;
  int			__length;

  __length = read(this->__fd, this->__buffer, BUF_LEN);
  
  for (__p = this->__buffer; __p < this->__buffer + __length;)
    {
      std::map<uint32_t, Function>::iterator	__it;
      __event = (struct inotify_event *)	__p;

      for (__it = this->__funcEvent.begin(); __it != this->__funcEvent.end(); ++__it)
	if (((*__it).first & __event->mask) && !(__event->mask & IN_ISDIR))
	  (this->*__funcEvent[__event->mask])(__event);
	  
      __p += sizeof(struct inotify_event) + __event->len;
    }

  sleep(this->__refreshTime); 
}

void Notify::eventMoveToFile(struct inotify_event *event)
{
  std::string __file(event->name);
  this->preparePacket(__file, MOVED_TO);
  this->__logManager->logMoveToFile(event);
}

void Notify::eventMoveFromFile(struct inotify_event *event)
{
  std::string __file(event->name);
  this->preparePacket(__file, MOVED_FROM);
  this->__logManager->logMoveFromFile(event);
}

void Notify::eventCreateFile(struct inotify_event *event)
{
  std::string __file(event->name);

  /*  if (__file ==  this->__logManager->getHome() + XML_CLIENT + this->__id + ".xml")
    this->preparePacket(__file, REFERENTIEL);
    else*/
    this->preparePacket(__file, CREATE);  

  this->__logManager->logCreateFile(event);
}

void Notify::eventDeleteFile(struct inotify_event *event)
{
  std::string __file(event->name);
  this->sendDelete(__file, DELETE);
  this->__logManager->logDeleteFile(event);
}

void Notify::eventModifyFile(struct inotify_event *event)
{
  std::string __file(event->name);
  this->preparePacket(__file, MODIFY);
  this->__logManager->logModifyFile(event);
}

void Notify::eventOpenFile(struct inotify_event *event)
{
  std::string __file(event->name);
  this->preparePacket(__file, "Open");
  this->__logManager->logOpenFile(event);
}

void Notify::eventAccesFile(struct inotify_event *event)
{
  std::string __file(event->name);
  this->preparePacket(__file, "Acces");
  this->__logManager->logAccesFile(event);
}


void Notify::preparePacket(const std::string &file, const std::string &action)
{
  if (this->__lastGZ != file)
    {
      std::string __xmlClient = this->__logManager->getHome() + XML_CLIENT + this->__id + ".xml";
      std::string __xmlServer = this->__logManager->getHome() + XML_SERVER;
      std::string __filePath = this->__logManager->getHome() + PATH_CLIENT + file;
      std::string __filePathGz = this->__logManager->getHome() + PATH_CLIENT + file + ".gz";
      
      this->__lastGZ = file + ".gz";
      std::string __contentFile = this->__streamManager->getStringFromFile(__filePath);
      
      this->__archiveManager->stringToGz(__contentFile, __filePathGz);
      
      //long nb_diff_referentiel = mycloud::xml::fonctionsxml::nb_differences_xml(xml_client, xml_serveur, idCompte);

      long int size = boost::filesystem::file_size(__filePath);
      
      std::stringstream sizeStringStream;
      sizeStringStream << size;
      std::string size2 = sizeStringStream.str();
      
      if (this->__xmlManager->isReadable(__xmlClient))
	{ 
	  this->__xmlManager->updateReferentielXml(__xmlClient,
						   this->__id,
						   file, //name
						   size2, //size
						   this->__logManager->getCurrentTime(), //date 
						   action); //action
	}
      else
	{    
	  this->__xmlManager->createReferentielXml(__xmlClient,
						   this->__id,
						   file, //name
						   size2, //size
						   this->__logManager->getCurrentTime(), //date 
						   action); //action__xmlClient,
	}
      
      std::string __contentGz = this->__streamManager->getStringFromFile(__filePathGz);

      remove(__filePathGz.c_str());      

      std::vector<Info>		__infos;
      Info			__file;

      __file.setName(file);
      __file.setContent(__contentGz);
      __file.setIdClient(this->__id);
      __file.setAction(action);
      __infos.push_back(__file);
      
      this->__communicationClient->newPacket(__infos);
      this->__logManager->logClientSendPacket(file);
    }
}

void Notify::sendDelete(const std::string &file, const std::string &action)
{
  std::string __xmlClient = this->__logManager->getHome() + XML_CLIENT + this->__id + ".xml";
 
  if (this->__xmlManager->isReadable(__xmlClient))
    { 
      this->__xmlManager->deleteElementOnFile(__xmlClient,
					    this->__id,
					    file);
    }
	
  std::vector<Info>    	__infos;
  Info			__file;
  
  __file.setName(file);
  __file.setIdClient(this->__id);
  __file.setAction(action);
  __infos.push_back(__file);
  
  this->__communicationClient->newPacket(__infos);
  this->__logManager->logClientSendPacket("Delete : " + file);
}

void Notify::getReferentielPacket(const std::string &action)
{
  std::string __xmlClient = this->__logManager->getHome() + XML_CLIENT + this->__id + ".xml";
  std::string __filePathGz = this->__logManager->getHome() + XML_CLIENT + this->__id + ".xml" + ".gz";
  
  long int size = 2;
  
  std::stringstream sizeStringStream;
  sizeStringStream << size;
  std::string size2 = sizeStringStream.str();
  
  if (this->__xmlManager->isReadable(__xmlClient))
    { 
      this->__xmlManager->updateReferentielXml(__xmlClient,
					       this->__id,
					       "referentiel_client_" + this->__id + ".xml", //name
					       size2, //size
					       this->__logManager->getCurrentTime(), //date 
					       action); //action
    }
  else
    {    
      this->__xmlManager->createReferentielXml(__xmlClient,
					       this->__id,
					       "referentiel_client_" + this->__id + ".xml", //name
					       size2, //size
					       this->__logManager->getCurrentTime(), //date 
					       action); //action__xmlClient,
    }

  std::string __contentFile = this->__streamManager->getStringFromFile(__xmlClient);
  
  this->__archiveManager->stringToGz(__contentFile, __filePathGz);
  std::string __contentGz = this->__streamManager->getStringFromFile(__filePathGz);  

  std::vector<Info>    	__infos;
  Info			__file;
  
  __file.setName("referentiel_client_" + this->__id + ".xml");
  __file.setContent(__contentGz);
  __file.setIdClient(this->__id);
  __file.setAction(action);
  __infos.push_back(__file);
  
  this->__communicationClient->newPacket(__infos);
  this->__logManager->logClientSendPacket("Get : " + action);
}

#include "Server.hpp"

Server::Server(StreamManager *streamManager, const std::string &home)
{
  std::cout << MAGENTA << "/* Welcome to DropBox Server */" << NCOLOR << std::endl 
	    << "type 'Help' for more informations" << std::endl;

  this->__streamManager = streamManager;
  this->__logManager	= new LogManager(TRACE_SERVER, home, streamManager);
  //this->__communicationServer = new CommunicationServer();
  this->initEventParser();
  this->serverEventListener();
}

Server::~Server() {}

void Server::initEventParser()
{
  this->__func["start"]		= &Server::eventStartServer;
  this->__func["stop"]		= &Server::eventStopServer;
  this->__func["status"]       	= &Server::eventStatusServer;
  this->__func["log"]		= &Server::eventLogServer;
  this->__func["help"]		= &Server::eventHelpServer;
  this->__func["exit"]		= &Server::eventExitServer;
}

void Server::serverEventListener()
{
  bool						__foundCmd;
  std::string					__cmd;
  std::map<std::string, Function>::iterator	__it;

  __foundCmd = false;
  __cmd = this->__streamManager->getStreamFromTerm(false, "Dropbox Server");
  boost::algorithm::to_lower(__cmd);

  this->__lastCmd = __cmd;

  for (__it = this->__func.begin(); __it != this->__func.end(); ++__it)
    if ((*__it).first == __cmd)
      {
	this->__logManager->logCmdUser(__cmd);
	(this->*__func[__cmd])();
	__foundCmd = true;
      }

  if (!__foundCmd)
    this->eventCmdNotFound(__cmd);

  this->serverEventListener();
}

void Server::eventHelpServer()
{ 
  std::cout << MAGENTA << "/* DropBox Server Help */" << NCOLOR << std::endl 
	    << "type 'Start' for launch server" << std::endl 
	    << "type 'Stop' for stop server" << std::endl 
	    << "type 'Status' for stop server" << std::endl 
	    << "type 'Log' for view log server" << std::endl
	    << "type 'Exit' for exit server" << std::endl;
}

void Server::eventStatusServer()
{
  std::cout << MAGENTA << "/* DropBox Server Status */" << NCOLOR << std::endl; 

  if (this->__communicationServer)
    {     
      std::cout << "Status Server :" << GREEN << " ONLINE" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "ONLINE");
    }
  else
    {
      std::cout << "Status Server :" << RED << " OFFLINE" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "OFFLINE");
    }
}

void Server::eventStartServer()
{
  std::cout << MAGENTA << "/* DropBox Server Start */" << NCOLOR << std::endl;
  XmlManager *__xmlManager;  
  
  if (this->__communicationServer == NULL)
    {
      __xmlManager = new XmlManager();
      this->__communicationServer = new CommunicationServer(this->__logManager, this->__streamManager, __xmlManager);
      this->__serverThread = new boost::thread(&CommunicationServer::launchServer, this->__communicationServer);
      std::cout << "Starting success ;)." << std::endl
		<< "Status Server :" << GREEN << " ONLINE" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "SUCCESS");
      this->__logManager->logCmdUser(this->__lastCmd, "ONLINE");
    }
  else
    {
      std::cout << "Failed : Server is already up." << std::endl
		<< "Status Server :" << GREEN << " ONLINE" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "FAIL : ALREADY UP");
      this->__logManager->logCmdUser(this->__lastCmd, "ONLINE");
    }
}

void Server::eventStopServer()
{
  std::cout << MAGENTA << "/* DropBox Server Stop */" << NCOLOR << std::endl;

  this->__communicationServer->stopServer();
  this->__communicationServer = NULL;
  this->__serverThread->interrupt();

  std::cout << "Stop success ;)." << std::endl
	    << "Status Server :" << RED << " OFFLINE" << NCOLOR << std::endl;
  this->__logManager->logCmdUser(this->__lastCmd, "SUCCESS");
  this->__logManager->logCmdUser(this->__lastCmd, "OFFLINE");
}

void Server::eventLogServer()
{
  std::cout << MAGENTA << "/* DropBox Server Log */" << NCOLOR << std::endl;

  this->__logManager->logCmdUser(this->__lastCmd, "SUCCESS");

  std::list<std::string>	__log = this->__streamManager->getListFromFile(TRACE_SERVER);
  int				__count = LOG_LINE;

  __log.reverse();

  for (std::string __logLine : __log)
    {
      if (__count == 0)
	break;
      
      std::cout << __logLine << std::endl;
      __count--;
    }
}

void Server::eventExitServer()
{
   std::cout << MAGENTA << "/* DropBox Server Exit*/" << std::endl 
	     << "/* Good Bye ;) */" << NCOLOR << std::endl;
   this->__logManager->logCmdUser(this->__lastCmd, "BYE ;)");
  exit(0);
}

void Server::eventCmdNotFound(const std::string &cmd)
{
  std::cout << "Wrong Command type : \"" << cmd << "\"." << std::endl 
	    << "type 'Help' for more informations" << std::endl;;
  this->__logManager->logCmdUser(this->__lastCmd, "COMMAND NOT FOUND");
}


#include "Client.hpp"

Client::Client(StreamManager *streamManager, const std::string &home) 
{
  std::cout << MAGENTA << "/* Welcome to DropBox Client */" << NCOLOR << std::endl 
	    << "type 'Help' for more informations" << std::endl;

  this->__streamManager	= streamManager;
  this->__home		= home;
  this->__logManager	= new LogManager(TRACE_CLIENT, this->__home, streamManager);
  this->__isLogin	= false;
  this->__isNotify	= false;
  this->getMyCloudAccesConfig();
  this->initEventParser();
  this->clientEventListener();
}

Client::~Client() {}

void Client::getMyCloudAccesConfig()
{
  std::list<std::string>	__fileConfig;
  std::string			__type;

  __fileConfig = this->__streamManager->getListFromFile(".MyCloud/MyCloud.config");

  for (std::string content : __fileConfig)
    {
      std::istringstream	__ss(content);
      
      __ss >> __type;
      if (__type == "rootLogin=")
	__ss >> this->__rootLogin;
      else if (__type == "rootPassword=")
	__ss >> this->__rootPassword;
      else if (__type == "database=")
	__ss >> this->__database;
    }
  std::cout << "Database : " << this->__database << ", rootlogin : " << this->__rootLogin << ", rootpassword : " << this->__rootPassword << std::endl;
}

void Client::initEventParser()
{
  this->__func["login"]      	= &Client::eventLoginClient;
  this->__func["disconnect"]   	= &Client::eventDisconnectClient;
  this->__func["synchonize"]   	= &Client::eventSynchronizeClient;
  this->__func["start"]   	= &Client::eventStartNotifyClient;
  this->__func["stop"]   	= &Client::eventStopNotifyClient;
  this->__func["status"]   	= &Client::eventStatusClient;
  this->__func["createuser"]	= &Client::eventCreateUserClient;
  this->__func["log"]		= &Client::eventLogClient;
  this->__func["help"]		= &Client::eventHelpClient;
  this->__func["exit"]		= &Client::eventExitClient;
}

void Client::clientEventListener()
{
  std::map<std::string, Function>::iterator	__it;
  bool						__foundCmd;
  std::string					__cmd;

  __foundCmd	= false;
  __cmd		= this->__streamManager->getStreamFromTerm(false, "Dropbox Client");
  boost::algorithm::to_lower(__cmd);

  this->__lastCmd = __cmd;

  for (__it = this->__func.begin(); __it != this->__func.end(); ++__it)
    if ((*__it).first == __cmd)
      {
	this->__logManager->logCmdUser(__cmd);
	(this->*__func[__cmd])();
	__foundCmd = true;
      }

  if (!__foundCmd)
    this->eventCmdNotFound(__cmd);

  this->clientEventListener();
}

/* VIEW */

void Client::eventHelpClient()
{
  std::cout << MAGENTA << "/* DropBox Client Help */" << NCOLOR << std::endl 
	    << "type 'Login' for connect user." << std::endl 
	    << "type 'Disconnect' for disconnect user." << std::endl 
	    << "type 'Synchronize' for synchronize user repo." << std::endl 
	    << "type 'Start' for start notify client." << std::endl 
	    << "type 'Stop' for stop notify client." << std::endl
    	    << "type 'Status' for status notify client." << std::endl 
	    << "type 'CreateUser' for create user client." << std::endl 
	    << "type 'Config' for configuration of db client." << std::endl 
	    << "type 'Log' for view log client" << std::endl
	    << "type 'Exit' for exit client" << std::endl;
}

void Client::eventLoginClient()
{
  std::cout << MAGENTA << "/* DropBox Client Login */" << NCOLOR << std::endl;

  this->loginClient(TRY_PASSWORD);
}

void Client::eventDisconnectClient()
{
  std::cout << MAGENTA << "/* DropBox Client Disconnect*/" << NCOLOR << std::endl;

  if (this->checkUserLogin())
    this->disconnectClient();
}

void Client::eventSynchronizeClient()
{
  std::cout << MAGENTA << "/* DropBox Client Disconnect*/" << NCOLOR << std::endl;

  if (this->checkUserLogin())
    this->__notify->getReferentielPacket(REFERENTIEL);
}

void Client::eventCreateUserClient()
{
  std::cout << MAGENTA << "/* DropBox Client Create User*/" << NCOLOR << std::endl;

  this->createUserClient();
}

void Client::eventStartNotifyClient()
{
  std::cout << MAGENTA << "/* DropBox Start Client*/" << NCOLOR << std::endl;

  if (this->checkUserLogin())
    this->startNotifyClient();
}

void Client::eventStopNotifyClient()
{
  std::cout << MAGENTA << "/* DropBox Stop Client*/" << NCOLOR << std::endl;

  if (this->checkUserLogin())
    this->stopNotifyClient();
}

void Client::eventStatusClient()
{
  std::cout << MAGENTA << "/* DropBox Status Client*/" << NCOLOR << std::endl;

  if (this->__isNotify)
    {
      std::cout << "Status Client :" << GREEN << " ONLINE" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "ONLINE");
    }
  else
    {
      std::cout << "Status Client :" << RED << " OFFLINE" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "OFFLINE");
    }
}

void Client::eventLogClient()
{
  std::cout << MAGENTA << "/* DropBox Client Log*/" << NCOLOR << std::endl;

  if (this->checkUserLogin())
    {
      this->getLogFromTrace();
      this->__logManager->logCmdUser(this->__lastCmd, "SUCCESS");
    }
}

void Client::eventExitClient()
{
  std::cout << MAGENTA << "/* DropBox Client Exit*/"  << NCOLOR << std::endl 
	    << MAGENTA << "/* Good Bye ;) */" << NCOLOR << std::endl;

  this->__logManager->logCmdUser(this->__lastCmd, "BYE ;)");
  exit(1);
}

void Client::eventCmdNotFound(const std::string &cmd)
{
  std::cout << RED << "Wrong Client Command type : \"" << cmd << "\"." << NCOLOR << std::endl 
	    << "type 'Help' for more informations" << std::endl;

  this->__logManager->logCmdUser(this->__lastCmd, "COMMAND NOT FOUND");
}

bool Client::checkUserLogin()
{
  if (this->__isLogin)
    {
      return true;
    }
  else
    {
      std::cout << RED << "Please login for use this command : " 
		<< this->__lastCmd <<  NCOLOR << std::endl
		<< "Type 'Login' and enter your username and password for use MyCloud" << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "ACCESS DENIED");
       return false;
    }
}

/* CONTROLLER */

void Client::loginClient(int tryPassword)
{
  std::string __login;
  std::string __password;

  __login	= this->__streamManager->getStreamFromTerm(false, "Username");
  __password	= this->__streamManager->getStreamFromTerm(true, "Password");
  std::cout << std::endl;
  
  if (this->matchUserDB(__login, __password))
    {
      std::cout << GREEN << "Login success, you can now use MyCloud app" << NCOLOR << std::endl;
      this->__isLogin = true;
      this->__logManager->logCmdUser(this->__lastCmd, "SUCCES");
    }
  else if (tryPassword != 0)
    {
      std::cout << RED << "Login failed : " << tryPassword 
		<< " more chance" << NCOLOR<< std::endl;
      this->loginClient(tryPassword - 1);
      this->__logManager->logCmdUser(this->__lastCmd, "FAIL");
    }
}

void Client::disconnectClient()
{
  std::cout << GREEN << "Disconnect success, relogin to use MyCloud app" << NCOLOR << std::endl;
  this->__isLogin = false;
  this->__logManager->logCmdUser(this->__lastCmd, "SUCCES");
}

void Client::stopNotifyClient()
{
  std::cout << "Status Client :" << RED << " OFFLINE" << NCOLOR << std::endl;
  this->__isNotify = false;
  this->__logManager->logCmdUser(this->__lastCmd, "OFFLINE");
  this->__notify->stopNotify();
}

void Client::startNotifyClient()
{
  this->__notify	= new Notify(TIME_REFRESH, this->__id, this->__logManager, this->__streamManager);
  this->__isNotify	= true;

  std::cout << "Status Client :" << GREEN << " ONLINE" << NCOLOR << std::endl;
  this->__logManager->logCmdUser(this->__lastCmd, "ONLINE");
  //this->__notifyThread = new boost::thread(&Notify::startNotify, 
  //				   this->__notify);
}

bool Client::matchUserDB(const std::string &user, const std::string &password)
{  
  db::DataBaseType<db::MySQL> *__db = this->getDB();

  this->__id = __db->getUser(user, password);

  if (this->__id != "")
    {
      this->__login = __login;
      return true;
    }  
  
  delete __db;
  return false;
}

void Client::createUserClient()
{
  db::DataBaseType<db::MySQL>	*__db = this->getDB();
  std::string			__login;
  std::string			__password;

  __login	= this->__streamManager->getStreamFromTerm(false, "Login");
  __password	= this->__streamManager->getStreamFromTerm(true, "Password");
  
  __db->createUser(__login, __password);
 
  if (__db->getUser(__login, __password) != "")
    {
      std::cout << GREEN << "Create successed" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "SUCCES");
    }
  else
    {
      std::cout << RED << "Create failed" << NCOLOR << std::endl;
      this->__logManager->logCmdUser(this->__lastCmd, "FAIL");
    }

  delete __db;
}

db::DataBaseType<db::MySQL> *Client::getDB()
{
  return (new db::DataBaseType<db::MySQL>(this->__rootLogin, 
					 this->__rootPassword, 
					  this->__database));
}

void Client::getLogFromTrace()
{
  std::list<std::string>	__log = this->__streamManager->getListFromFile(TRACE_CLIENT);
  int				__count = LOG_LINE;

  __log.reverse();

  for (std::string __logLine : __log)
    {
      if (__count == 0)
	break;
      
      std::cout << __logLine << std::endl;
      __count--;
    }
}

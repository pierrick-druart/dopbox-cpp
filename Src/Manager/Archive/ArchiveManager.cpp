#include "ArchiveManager.hpp"

ArchiveManager::ArchiveManager() {}

ArchiveManager::~ArchiveManager() {}

void ArchiveManager::gzToString(std::string &data_out, const std::string &path_gz)
{
  std::stringstream data;
  std::ifstream file(path_gz.c_str(), std::ifstream::binary);
  boost::iostreams::filtering_streambuf< boost::iostreams::input > in;
  in.push(boost::iostreams::gzip_decompressor());
  in.push(file);
  boost::iostreams::copy(in, data);
  data_out = data.str();
}
 
void ArchiveManager::stringToGz(const std::string &data_in, const std::string &path_gz)
{
  std::stringstream data;
  std::ofstream file(path_gz.c_str(), std::ofstream::binary);
  boost::iostreams::filtering_streambuf< boost::iostreams::input > in;
  in.push(boost::iostreams::gzip_compressor());
  data << data_in;
  
  in.push(data);
  boost::iostreams::copy(in, file);
}

#include "StreamManager.hpp"

StreamManager::StreamManager() {}

StreamManager::~StreamManager() {}

void StreamManager::HideTerminalInput(bool hide)
{
  struct termios	__tty;
  
  tcgetattr(STDIN_FILENO, &__tty);
  
  if(hide)
    __tty.c_lflag &= ~ECHO;
  else
    __tty.c_lflag |= ECHO;
  
  (void) tcsetattr(STDIN_FILENO, TCSANOW, &__tty);
}

std::string StreamManager::getStreamFromTerm(bool password, const std::string &prompt)
{
  std::string		__flux;
  
  if (prompt != "")
    std::cout << prompt << " : ";

  this->HideTerminalInput(password);
  getline(std::cin, __flux);
  this->HideTerminalInput(false);

  return __flux;
}
 
std::list<std::string> StreamManager::getListFromFile(const std::string &file)
{
  std::list<std::string>	__stream;
  std::string			__line;
  const char			*__fileName = file.c_str();
  std::ifstream			__streamFile(__fileName);
  
  try
    { 
      if (__streamFile.is_open())
	{
	  while (__streamFile.good())
	    {
	      getline(__streamFile, __line);
	      __stream.push_back(__line);
	    }
	  __streamFile.close();
	}
      else
	throw my_exception_file(file);
      
    }
  catch (const std::exception &e)
    {
      std::cerr << "StreamManager [" << e.what() << "]" << std::endl;
    }

  return __stream;
}

std::string StreamManager::getStringFromFile(const std::string &file)
{
  std::string			__stream;
  std::string			__line;
  const char			*__fileName = file.c_str();
  std::ifstream			__streamFile(__fileName);
  
  try
    { 
      if (__streamFile.is_open())
	{
	  while (__streamFile.good())
	    {
	      getline(__streamFile, __line);
	      __stream = __stream + __line + "\n";
	    }
	  __streamFile.close();
	}
      else
	throw my_exception_file(file);
      
    }
  catch (const std::exception &e)
    {
      std::cerr << "StreamManager [" << e.what() << "]" << std::endl;
    }

  return __stream;
}

void StreamManager::setContentsOnFile(std::list<std::string> contents, const std::string &fileName)
{
  std::list<std::string>::iterator	__itContents;

  std::ofstream			        __file(fileName.c_str(), std::ios::out | std::ios::app);
  
  try
    { 
      if (__file.is_open())
	{
	  for (__itContents = contents.begin(); __itContents != contents.end(); ++__itContents)
	    __file << (*__itContents) << std::endl;
	}
      else
	throw my_exception_file(fileName);
      
      __file.close();
    }
  catch (const std::exception &e)
    {
      std::cerr << "StreamManager [" << e.what() << "]" << std::endl;
    }
}

void StreamManager::setContentOnFile(std::string content, const std::string &fileName)
{
  std::ofstream			        __file(fileName.c_str(), std::ios::out | std::ios::app);
  
  try
    { 
      if (__file.is_open())
	{
	  __file << content << std::endl;;
	}
      else
	throw my_exception_file(fileName);
      
      __file.close();
    }
  catch (const std::exception &e)
    {
      std::cerr << "StreamManager [" << e.what() << "]" << std::endl;
    }
}

void StreamManager::replaceContentOnFile(std::string content, const std::string &fileName)
{
  std::ofstream			        __file(fileName.c_str(), std::ios::out | std::ios::trunc);
  
  try
    { 
      if (__file.is_open())
	{
	  __file << content << std::endl;;
	}
      else
	throw my_exception_file(fileName);
      
      __file.close();
    }
  catch (const std::exception &e)
    {
      std::cerr << "StreamManager [" << e.what() << "]" << std::endl;
    }
}

void StreamManager::setContentOnFileGZ(std::string content, const std::string &fileName)
{
  std::ofstream			        __file(fileName.c_str(), std::ios::out | std::ios::app);
  
  try
    { 
      if (__file.is_open())
	{
	  __file << content;
	}
      else
	throw my_exception_file(fileName);
      
      __file.close();
    }
  catch (const std::exception &e)
    {
      std::cerr << "StreamManager [" << e.what() << "]" << std::endl;
    }
}

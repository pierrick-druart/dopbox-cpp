#ifndef __INFO_H__
#define __INFO_H__

#include <string>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/detail/decl.hpp>

class Info 
{
private:
  std::string	__path;
  std::string	__name;
  std::string  	__size;
  std::string	__content;
  std::string	__action;
  std::string	__idClient;
  std::string	__date;
  
private:
 friend class boost::serialization::access;
  template<class Archive> void serialize(Archive & ar, const unsigned int version) 
  {
    ar & __path;
    ar & __name;
    ar & __size;
    ar & __content;
    ar & __action;
    ar & __idClient;
    ar & __date;
  }
  
public:
  Info();
  ~Info();
  
public:
  const std::string &getPath() const;
  void setPath(const std::string &path);
  const std::string &getName() const;
  void setName(const std::string &name);
  const std::string &getSize() const;
  void setSize(const std::string &size);
  const std::string &getContent() const;
  void setContent(const std::string &content);
  const std::string &getAction() const;
  void setAction(const std::string &action);
  const std::string &getIdClient() const;
  void setIdClient(const std::string &idClient);
  const std::string &getDate() const;
  void setDate(const std::string &date);
};

#endif /* INFO_H_ */
